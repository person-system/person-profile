/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.repos;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import lombok.Getter;
import ru.miroque.pp.domain.Personality;
import ru.miroque.pp.domain.Sex;

@ApplicationScoped
public class RepoPersonality {

	@Getter
	private List<Personality> personalities = new ArrayList<Personality>();

	@PostConstruct
	private void initList() {
		Personality _01 = new Personality(Sex.MALE, "Jone Doe");
		personalities.add(_01);
		Personality _02 = new Personality(Sex.MALE, "Иван Добрый");
		personalities.add(_02);
		Personality _03 = new Personality(Sex.FEMAIL, "Ксения Умильная");
		personalities.add(_03);
		Personality _04 = new Personality(Sex.FEMAIL, "Ольга Стрежная");
		personalities.add(_04);

	}

}
