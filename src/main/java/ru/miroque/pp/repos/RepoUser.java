/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.repos;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import lombok.Getter;
import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.domain.DefaultSeed;
import ru.miroque.pp.domain.Node;
import ru.miroque.pp.domain.User;

@JBossLog
@ApplicationScoped
public class RepoUser {

	@Getter
	private List<User> users = new ArrayList<>();

	@Inject
	private RepoPersonality repoPersonality;

	@PostConstruct
	private void initList() {
	    log.info("RepoUser initList()");
		// Person 1
	    
	    List<Node> nodes_Prog_Lang = new ArrayList<>();
	    Node m_01 = new Node(UUID.randomUUID(), new DefaultSeed(repoPersonality.getPersonalities().get(0).getFIO(), 0), null, nodes_Prog_Lang);
	    
	    Node leaf_Pgor = new Node(UUID.randomUUID(), new DefaultSeed("Prog", 20), m_01, null);
	    Node leaf_Lang = new Node(UUID.randomUUID(), new DefaultSeed("Lang", 60), m_01, null);
	    nodes_Prog_Lang.add(leaf_Pgor);
	    nodes_Prog_Lang.add(leaf_Lang);
	    
	    
		User _01 = new User(UUID.randomUUID(), repoPersonality.getPersonalities().get(0), m_01);
		users.add(_01);
		
		// Person 2
		Node m_02 = new Node();
		User _02 = new User(UUID.randomUUID(), repoPersonality.getPersonalities().get(1), m_02);
		users.add(_02);
		
		// Person 3
		Node m_03 = new Node();
		User _03 = new User(UUID.randomUUID(), repoPersonality.getPersonalities().get(2), m_03);
		users.add(_03);
		
		// Person 4
		Node m_04 = new Node();
		User _04 = new User(UUID.randomUUID(), repoPersonality.getPersonalities().get(3), m_04);
		users.add(_04);
	}

	public User getUser(UUID idUser) {
		return users.stream().filter(u -> u.getId().equals(idUser)).findFirst().get();
	}

}
