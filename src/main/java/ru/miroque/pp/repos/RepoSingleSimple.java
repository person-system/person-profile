/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.repos;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.domain.DataItem;
import ru.miroque.pp.domain.DefaultSeed;
import ru.miroque.pp.domain.Node;
import ru.miroque.pp.domain.Personality;
import ru.miroque.pp.domain.Sex;
import ru.miroque.pp.domain.User;

/**
 * Только один пользователь ! и всё. он будет источником данных, 
 * @author a-panov
 *
 */
@JBossLog
@ApplicationScoped
public class RepoSingleSimple {

    User user;

	@PostConstruct
	private void initList() {
	    log.info("RepoSingleSimple init()");
	    UUID id = UUID.fromString("00000000-0000-0000-0000-000000000001");
	    
	    Personality identity = new Personality(UUID.fromString("10000000-0000-0000-0000-000000000001"), Sex.MALE, "Jone-Doe");
	    DataItem data = new DefaultSeed("root", 0);
		
	    List<Node> children = new ArrayList<>();
		Node map = new Node(id, data, null, children);
		
		List<Node> _programer_children = new ArrayList<>();
        Node _programer = new Node(UUID.fromString("00000000-0000-0000-0001-000000000001"), new DefaultSeed("proger", 0), map, _programer_children);
        
        Node _javase = new Node(UUID.fromString("00000000-0000-0001-0001-000000000001"), new DefaultSeed("Java SE", 80), _programer, null);
        Node _python = new Node(UUID.fromString("00000000-0000-0002-0001-000000000001"), new DefaultSeed("Python 3", 15), _programer, null);
        _programer_children.add(_javase);
        _programer_children.add(_python);
        
		Node _artist = new Node(UUID.fromString("00000000-0000-0000-0002-000000000001"), new DefaultSeed("artist", 10), map, null);
		children.add(_programer);
		children.add(_artist);
		user  = new User(id, identity, map);
	}

	public User getUser(UUID idUser) {
		return user;
	}

	public Node getNode(UUID id) {
		return user.findNode(id);
	}

    public Personality getPersonality(String id) {
        if (user.getIdentity().getId().equals(UUID.fromString(id))) {
            return user.getIdentity();
        } else {
            return null;
        }
    }

}
