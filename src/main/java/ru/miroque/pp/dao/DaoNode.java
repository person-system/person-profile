/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.dao;

import java.io.Serializable;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;

import ru.miroque.pp.domain.Node;
import ru.miroque.pp.repos.RepoUser;

@RequestScoped
public class DaoNode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private RepoUser users;

	public Node getNode(UUID idUser, UUID id, Integer depth) {
		return users.getUser(idUser).findNode(id) ;
	}
	
	public Node getNode(UUID idUser, UUID id) {
		return getNode(idUser, id, 1) ;
	}

}
