/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.etc;

import java.io.Serializable;

public class Limits implements Serializable {
    private static final long serialVersionUID = 1L;
    public int page; // e.g. 0 page means out of range
    public int pages;
    public int tableRowsCapacity; // e.g. 10, 20, 50, 100
    public int tableRowsTotal;

    public Limits() {
        super();
    };

    public Limits(int page, int pages, int tableRowsCapacity, int tableRowsTotal) {
        this.page = page;
        this.pages = pages;
        this.tableRowsCapacity = tableRowsCapacity;
        this.tableRowsTotal = tableRowsTotal;
    }
}
