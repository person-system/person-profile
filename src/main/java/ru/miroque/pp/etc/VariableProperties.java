/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.etc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class VariableProperties {
	public String getServerVersion() {
		String resourceName = "application.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			try {
				props.load(resourceStream);
			} catch (IOException e) {
				log.error("exeption ", e);
			}
			return props.getProperty("version");
		} catch (IOException e1) {
			log.error("exeption ", e1);
		}
		return "";
	}
}
