/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.services;

import java.util.UUID;

import javax.inject.Inject;

import ru.miroque.pp.dao.DaoNode;
import ru.miroque.pp.domain.Node;


public class ServiceNode {
	
	@Inject
	private DaoNode dao;

	public Node getNode(UUID idUser, UUID idNode) {
		return dao.getNode(idUser, idNode);
	}

	public Node getNode(UUID idUser, UUID idNode, Integer depth) {
		return dao.getNode(idUser, idNode, depth);
	}


}
