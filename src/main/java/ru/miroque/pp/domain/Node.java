/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.domain;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author panov Класс для хранения информации в узле дерева... Нужен ли
 *         параметр, или сразу можно всё указать, или можно параметром
 *         передавать подготовленный класс, с лейблами и значениями, и весами?
 * 
 *         хм.. жалко сейчас не вижу...
 * @param <T>
 */
@AllArgsConstructor
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class Node implements Serializable {
	private static final long serialVersionUID = 1L;
	@XmlID
	private UUID              id;
	private DataItem          data;
	@XmlTransient
	private Node              parent;
	private List<Node>        children;

	public Node() {
		id = UUID.randomUUID();
	}

	boolean isNode() {
		return !isLeaf() && !isRoot();
	}

	boolean isLeaf() {
		return children == null ? true : false;
	}

	boolean isRoot() {
		return parent == null ? true : false;
	}

	@XmlElement
	public Double getLevelOfNode() {
		Double result = 0.0;

		if (!isLeaf()) {
			int i = 0;
			double sum = 0.0;
			for (Node node : children) {
				sum += node.getLevelOfNode();
				i++;
			}
			result = sum / i;
		} else {
			if (data != null && data.getValue() != null) {
				result = data.getValue().doubleValue();
			}
		}

		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Node");
		sb.append(" [id=").append(id).append(", data=").append(data != null ? data : "NA").append(", parent=DataItem::")
				.append((parent != null ? parent.getData().getLabel() : "root")).append(", children=")
				.append(children != null ? children.toString() : "NA").append("]");
		return sb.toString();
	}

	/**
	 * Вообще-то, походу дела... эту ноду надо вообще держать по минимому, это как квант всей штуки.
	 * для неё то и указывается параметр "глубина" т.е. на сколько узлов вниз я могу ее собирать
	 * @param id
	 * @return
	 */
	public Node findNode(UUID id) {
		if (this.id.equals(id)) {
			return this;
		} else {
			for (Node item : children) {
				if (item.findNode(id) != null) {
					return item.findNode(id);
				} else {
					return null;
				}
			}
			return null;
		}
	}

}
