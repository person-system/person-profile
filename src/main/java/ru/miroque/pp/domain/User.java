/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.domain;

import java.util.UUID;

import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@XmlRootElement
@ToString
@EqualsAndHashCode
public class User {
	@Getter
	private final UUID        id;
	@Getter
	@XmlIDREF
	private final Personality identity;
	// TODO Media media;
	
	@Getter
	//HINT: https://stackoverflow.com/a/38877646/1679702
	@XmlIDREF
	private Node map;

	public User() {
		this(UUID.randomUUID(), new Personality(Sex.MALE, "Jone Doe"), null);
	}

	public User(Node map) {
		this(UUID.randomUUID(), new Personality(Sex.MALE, "Jone Doe"), map);
	}

	public User(UUID id, Personality identity, Node map) {
		this.id = id;
		this.identity = identity;
		this.map = map;
	}

	public Node findNode(UUID id) {
		return map.findNode(id);
	}

}
