/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.domain;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Personality - Персональные данные.
 * 
 * @author <a href="mailto:i.miroque@gmail.com">Panov Aleksander</a>
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Personality {
    @XmlID
    @Getter @Setter
    private UUID id;
    // TODO: {f: x, i:x, o:x}
    @Getter @Setter
    private Sex sex;
    // TODO: birthDate: x.x.x
    private final String name;

    public Personality() {
        id = UUID.randomUUID();
        name = "NA";
        sex = Sex.OTHER;
    }

    public Personality(Sex sex, String name) {
        id = UUID.randomUUID();
        this.name = name;
        sex = Sex.OTHER;
    }

    public String getFIO() {
        return name;
    }
}
