/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.rest;

import java.security.Key;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.domain.Personality;
import ru.miroque.pp.domain.User;
import ru.miroque.pp.filters.JWTTokenNeeded;
import ru.miroque.pp.repos.RepoSingleSimple;
import ru.miroque.pp.util.KeyGenerator;

//TODO: !!!!!!!!!!!! READ !!!!!!!!! and do!!!!!!!!!! https://bitbucket.org/b_c/jose4j/wiki/JWT Examples

@Api(protocols = "http", value = "Personality")
@JBossLog
@Path("personality")
public class RestPersonality {

	@Inject
	private RepoSingleSimple repo;

	@Inject
	private KeyGenerator keyGenerator;

	@ApiOperation(value = "Finds user`s personality", notes = "Some awesome note", response = Personality.class)
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@JWTTokenNeeded
	public Response getPersonality(@HeaderParam("Authorization") String authorizationHeader, @PathParam("id") String id) {
		log.info("->getPersonality<>id: " + id);
		String token = authorizationHeader.substring("Bearer".length()).trim();
		if (token == null) {
			log.info("token null");
		} else {
			log.info("token: " + token);
			// Validate the token
			Key key = keyGenerator.generateKey();
			Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			log.info("#### valid token : " + token);
			Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
			String subj = claims.getSubject();
			log.info("#### subj : " + subj);
			String userId = claims.get("user-id").toString();
			log.info("#### userId : " + userId);
			// TODO: RETHINK THIS ALGO ↓
			// When I write this, I think it's the dumbiest thing to check user
			// rights to select this personality.....
			User user = repo.getUser(UUID.fromString(userId));
			log.debug("user:" + user);
			if (user.getIdentity().equals(repo.getPersonality(id))) {
				log.info("<-getPersonality");
				return Response.ok(user.getIdentity()).build();
			} else {
				log.warn("<-getPersonality");
				return Response.ok("{\"warning\":\"You can't get this personality.\"}").build();
			}

		}
		log.info("<-getPersonality");
		return Response.ok(null).build();
	}

}
