/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.domain.User;
import ru.miroque.pp.services.ServiceUser;

@Api(protocols="http", value="Users")
@JBossLog
@Path("users")
public class RestNode {

	@Inject
	private ServiceUser serviceUser;

	@ApiOperation(value="Finds user.", notes="Some awesome note", response=User.class)
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("id") String id) {
		log.info("->getUser<>id: " + id);
		User item = null;
		try {
			item = serviceUser.getUser(id);
			log.debug("  ::user:id: " + item.getId());
		} catch (Exception e) {
			log.error("em", e);
			log.info("<-getUser");
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		log.info("<-getUser");
		return Response.ok(item).build();
	}

}
