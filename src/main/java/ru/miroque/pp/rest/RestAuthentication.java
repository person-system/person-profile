/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.rest;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.util.KeyGenerator;

//TODO: !!!!!!!!!!!! READ !!!!!!!!! and do!!!!!!!!!! https://bitbucket.org/b_c/jose4j/wiki/JWT Examples


@Api(protocols="http", value="Personality")
@JBossLog
@Path("auth")
public class RestAuthentication {

    @Context
    private UriInfo uriInfo;
    
	@Inject
    private KeyGenerator keyGenerator;

	@ApiOperation(value="Suck jwt by client", notes="Some awesome note jwt")
	@GET
    @Path("/token")
    public Response authenticateUser() {
		log.info("->authenticateUser");
	    try {

//            log.info("#### login/password : " + login + "/" + password);

            // Authenticate the user using the credentials provided
//            authenticate(login, password);

            // Issue a token for the user
            String token = issueToken("default-user");

            // Return the token on the response
//            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();
            
            return Response.ok().entity("{\"token\": \""+token+"\"}").build();

        } catch (Exception e) {
            return Response.status(UNAUTHORIZED).build();
        }
    }
	
	private String issueToken(String login) {
		log.info("->issueToken");
        Key key = keyGenerator.generateKey();
        Claims claims = Jwts.claims();
        claims.put("user-id", "00000000-0000-0000-0000-000000000001");
        String jwtToken = Jwts.builder()
//                .setSubject(login)
                .setClaims(claims)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        log.info("#### generating token for a key : " + jwtToken + " - " + key);
        log.info("<-issueToken");
        return jwtToken;

    }
	  private Date toDate(LocalDateTime localDateTime) {
	        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	    }

}
