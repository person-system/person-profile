/*******************************************************************************
 * Person System - Person Profile module
 * Copyright (C) 2018, 2019 Panov Aleksander (i.miroque@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ru.miroque.pp.rest;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.jbosslog.JBossLog;
import ru.miroque.pp.etc.VariableProperties;

@Api(protocols="http", tags= {"Sever version"})
@JBossLog
@Path("about")
public class RestAbout {
	@Inject
	private VariableProperties properties;

	@ApiOperation(value="Depicts version of sever", notes="Some awesome note two")
	@GET
	@Path("version")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVersion() {
		log.info("->getVersion");
		Map<String, String> version = new HashMap<>(); 
		try {
			log.info("  ::" + properties.getServerVersion());
			version.put("version", properties.getServerVersion());
		} catch (Exception e) {
			log.error("em", e);
			log.info("<-getVersion");
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
		}
		log.info("<-getVersion");
		return Response.ok(version).build();
	}

}
