include make/make_env
.DEFAULT_GOAL := help

NS ?= helpcontrol
VERSION ?= latest
REPO = hc_person_profile
NAME = run_hc_person_profile
INSTANCE = default
MAKEFILE_LIST = Makefile

# NOTE: самодокументирование команд makefile
help: ## help - отображение списка доступных команд
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'


build: ## build docker image (с кэшированием)
	docker build --rm --force-rm -t $(NS)/$(REPO):$(VERSION)  -t $(NS)/$(REPO):latest -f Dockerfile .

rebuild: ## build docker image (--no-cache)
	docker build --rm --force-rm --no-cache -t $(NS)/$(REPO):$(VERSION)  -t $(NS)/$(REPO):latest -f Dockerfile .

start:	## start docker container
	docker run -d --name $(NAME)-$(INSTANCE) $(PORTS) $(VOLUMES) $(ENV) $(NS)/$(REPO):$(VERSION)

run: 	## run docker container
	docker run --rm --name $(NAME)-$(INSTANCE) $(PORTS) $(VOLUMES) $(ENV) $(NS)/$(REPO):$(VERSION)

shell: 	## run and open shell into docker container
	docker run --rm --name $(NAME)-$(INSTANCE) -i -t $(PORTS) $(VOLUMES) $(ENV) $(NS)/$(REPO):$(VERSION) /bin/bash

stop: 	## stop docker container
	docker stop $(NAME)-$(INSTANCE)

rm:		## remove docker container (container must be stoped at first)
	docker rm -f $(NAME)-$(INSTANCE)

deploy: ## deploy (only from host with access to remote host wia ssh)
	docker save $(NS)/$(REPO):$(VERSION) | bzip2 | pv | \
     ssh $(REMOTE_USER)@$(REMOTE_HOST) 'bunzip2 | docker load'

clean: ## remove all none images
	docker rmi $(docker images | grep "^<none>" | awk "{print $3}")

.PHONY: help build run shell stop rm deploy clean

