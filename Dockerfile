# @see https://hub.docker.com/_/maven
FROM maven:3.6.1-alpine AS build

LABEL authors="Aleksander Panov,Nikolay Gerzhan"

COPY src /usr/src/app/src
COPY pom.xml /usr/src/app

RUN mvn -f /usr/src/app/pom.xml clean package -Pthorntail

# @see https://hub.docker.com/_/openjdk
FROM openjdk:8-alpine
COPY --from=build /usr/src/app/target/pp-thorntail.jar /usr/app/pp-thorntail.jar

EXPOSE 8080

WORKDIR /usr/app/
ENTRYPOINT ["java","-Xms100m","-Xmx300m","-Djava.net.preferIPv4Stack=true","-jar","pp-thorntail.jar"]
