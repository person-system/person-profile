[![Hits-of-Code](https://hitsofcode.com/gitlab/person-system/person-profile)](https://hitsofcode.com/view/gitlab/person-system/person-profile)

# Person Profile

> Personal tool for control self level of education.

<dl>
<dt>Goal</dt>
<dd>Provide comfortable tool (online in Internet), for personal tracking of educational map.</dd>

<dt>Future goal</dt>
<dd>Some kind of personal helper for planning a path for improve skills and knowledge or refresh ones.</dd>
</dl>

## Development stack

### Requirements

Installed Java JDK 8 or later

Installed maven 3.4 or later

## Development run

```
mvn thorntail:run -P thorntail
```

**Unfortunately** there is no convenient way to stop the process, it should stops with `Ctrl+C`
but this not works.
For now is send `kill` to process.
With Task Manager in Windows or Linux `kill [PID]`

### Available addresses

Now sample data: available users 3(three)

    http://localhost:8080/api/users/1

## Swagger UI

Swagger definition by address `http://localhost:8080/api/swagger.json`

Swagger-UI by address `http://localhost:8080/swagger-ui` in explore field type swagger definition (see previous) and press `Explore`

## Config Eclipse IDE

`install lombok deps`

`select maven profile deps`

## Makefile

Help info

```bash
$make help
```

Make `docker image`

```bash
$make build
```

Deploy to VPS

```bash
$make deploy
```
